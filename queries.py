#!/usr/bin/env python2
from __future__ import print_function

import datetime
import pprint
import time
from datetime import date

import django
from django.db.models import Count
from django.db.models.aggregates import Sum

from woven.models import Page, ViewStat, SiteUser

django.setup()

if __name__ == "__main__":

    # (a) What are the total page views on each website for the past 7 days?
    # response:
    # { site1: num_views, site2: num_views, ..., siteN: num_views }

    PAST_DAYS = 21
    past_date_timestamp = time.time() - datetime.timedelta(days=PAST_DAYS).total_seconds()
    sites = ViewStat.objects.values_list('site', flat=True)
    response = {}
    for site in list(set(sites)):
        if site not in [None, '']:
            q = ViewStat.objects.filter(site__exact=site)\
                .filter(lastutc__gt=date.fromtimestamp(past_date_timestamp))\
                .aggregate(total_page_views=Sum('session_views'))
            response.update({
               site: q['total_page_views']
            })
    pprint.pprint(response)

    # answer:
    # past 7 days: no data
    # entire history: {u'brobible': 164119, u'uproxx': 126107}

    # (b) For a chosen website, what are the total page views for each day of the week?
    # https://docs.djangoproject.com/en/1.9/ref/models/querysets/#week-day
    # response:
    # { Sun: views1, Mon: views2, ... , Sat: viewsN }
    days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat']
    response = {}
    site = 'uproxx'
    for (index, day) in list(enumerate(days, start=1)):
        q = ViewStat.objects.filter(site__iexact=site)\
            .filter(lastutc__week_day=index)\
            .aggregate(total_page_views=Sum('session_views'))
        response.update({
            day: q['total_page_views']
        })
    pprint.pprint(response)

    # answers:
    # brobible: {'Fri': 22418, 'Mon': 33538, 'Sat': 10993, 'Sun': 14122, 'Thur': 26968, 'Tue': 29696, 'Wed': 26384}
    # uproxx: {'Fri': 15934, 'Mon': 20592, 'Sat': 16550, 'Sun': 16585, 'Thur': 17863, 'Tue': 15990, 'Wed': 22593}

    # (c) For a chosen day, what are the ten most popular pages?
    # response: [url1, url2, ..., urlN]
    # Q: count uproxx and brobible in the results or exclude?
    def sortkey(item):
        return item[1]

    TOP_N_PAGES = 10
    day = 2
    urls = ViewStat.objects.values_list('url', flat=True)
    page_views = []
    for url in list(set(urls)):
        q = ViewStat.objects.filter(url__exact=url) \
            .filter(lastutc__week_day=day)\
            .aggregate(total_page_views=Sum('session_views'))
        if q.get('total_page_views'):
            page_views += [(url, q['total_page_views'])]

    page_views.sort(key=sortkey)
    page_views.reverse()
    response = page_views[:TOP_N_PAGES]
    pprint.pprint(response)

    # (d) For a chosen page, what are the five most popular referrers?
    # response: [url1, url2, ..., urlN]
    print('POPULAR REFERRERS')
    TOP_N_REFERRERS = 5
    url = 'http://brobible.com/girls/article/elizabeth-hurley-on-instagram/'
    from collections import Counter
    from operator import itemgetter

    # get page referrers
    pagerefs = ViewStat.objects.filter(url__exact=url)\
        .exclude(pageref__exact='')\
        .values_list('pageref', flat=True)

    # (d)(1) Get the most frequent referrers
    print('BY FREQUENCY')

    # compute histogram dictionary
    pagerefs_counts = Counter(pagerefs)

    # sort dict by count value
    sorted_pagerefs = sorted(pagerefs_counts.items(), key=itemgetter(1), reverse=True)

    # get top referrers
    response = [p[0]
                for p in sorted_pagerefs[:TOP_N_REFERRERS]
                ]
    print(response)

    # (d)(2) Get the top-performing referrers (i.e. most page views)
    print('BY VIEWS')
    referred_views = []
    for pageref in set(pagerefs):
        q = ViewStat.objects.filter(pageref__exact=pageref)\
            .aggregate(total_page_views=Sum('session_views'))
        if q.get('total_page_views'):
            referred_views += [(pageref, q['total_page_views'])]

    referred_views.sort(key=sortkey)
    referred_views.reverse()
    response = referred_views[:TOP_N_REFERRERS]
    pprint.pprint(response)

    # (e) How many users are visiting multiple websites and what are they reading?
    # response:
    # [ { sha1: url }, ..., { shaN: url } ]

    response = [
        {
            str(u.id): Page.objects.filter(viewstats__usersha__exact=u.sha)
            .order_by('url')
            .values_list('url', flat=True)
        }
        for u in SiteUser.objects.all()
        if len(u.page_set.all()) > 1
    ]
    print('{} users viewing multiple pages. Here is what they are viewing:'
        .format(len(response)))
    pprint.pprint(response)
