import uuid

from django.core.validators import URLValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Beacon(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    request_log_id = models.CharField(db_index=True, max_length=128)
    timestamp = models.DateTimeField(auto_now=True)
    gae_project_name = models.CharField(max_length=255, blank=True)
    gae_instance_id = models.CharField(max_length=255, blank=True)
    gae_version_id = models.CharField(max_length=255, blank=True)
    status = models.IntegerField(blank=True)

    # Use IPv6 Validator
    remote_ip = models.CharField(max_length=40, blank=True, db_index=True,
                                 validators=[URLValidator()])
    version_tag = models.CharField(max_length=16, blank=True)
    json_block = models.CharField(max_length=2048, blank=True)
    b64_length = models.IntegerField(blank=True)

    class Meta:
        verbose_name = _('beacon')
        verbose_name_plural = _('beacons')

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return 'Beacon(id={}, request_log_id={}, remote_ip={}, status={})'\
            .format(repr(self.id), repr(self.request_log_id),
                    repr(self.remote_ip), repr(self.status),
                    )


class SiteUser(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sha = models.CharField(max_length=255, blank=True, unique=True)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return 'SiteUser(id={}, sha={})'.format(
            repr(self.id),
            repr(self.sha),
        )


class Site(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=1024, blank=True, unique=True)
    users = models.ManyToManyField(SiteUser)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return 'Site(id={}, name={})'.format(
            repr(self.id),
            repr(self.name),
        )


class Page(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    pageid = models.CharField(max_length=16, unique=True, blank=True, null=True, )
    url = models.CharField(max_length=1024, unique=True, blank=True, validators=[URLValidator()])
    site = models.ForeignKey(Site, related_name='pages',
                             on_delete=models.CASCADE, blank=False,
                             null=True)
    users = models.ManyToManyField(SiteUser)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        return 'Page(id={}, pageid={}, url={})'.format(
            repr(self.id),
            repr(self.pageid),
            repr(self.url),
        )


class ViewStat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    url = models.CharField(max_length=1024, blank=True, validators=[URLValidator()])
    pageref = models.CharField(max_length=1024, blank=True, validators=[URLValidator()])
    site = models.CharField(max_length=1024, blank=True)
    usersha = models.CharField(max_length=255, blank=True)
    lastutc = models.DateTimeField(blank=True, null=True)
    session_views = models.CharField(max_length=16, blank=True, null=True)
    lifetime_views = models.CharField(max_length=16, blank=True, null=True)
    page = models.ForeignKey(Page, related_name='viewstats',
                             on_delete=models.CASCADE, blank=False,
                             null=True)

    class Meta:
        ordering = ['session_views']
