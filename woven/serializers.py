from rest_framework import serializers

from woven.models import (Site, Page, SiteUser, ViewStat, )


class SiteUserSerializer(serializers.ModelSerializer):
    """Serializer assuming the default REST framework handling capabilities"""
    class Meta:
        model = SiteUser
        fields = ('id', 'sha', )


class PageSerializer(serializers.ModelSerializer):
    """Serializer assuming the default REST framework handling capabilities"""
    class Meta:
        model = Page
        fields = ('id', 'pageid', 'site', 'url', )


class SiteSerializer(serializers.ModelSerializer):
    """Serializer assuming the default REST framework handling capabilities"""
    class Meta:
        model = Site
        fields = ('id', 'name', )


class ViewStatSerializer(serializers.ModelSerializer):
    """Serializer assuming the default REST framework handling capabilities"""
    class Meta:
        model = ViewStat
        fields = ('id', 'lastutc', 'lifetime_views', 'page', 'pageref',
                  'session_views', 'site', 'url', 'usersha', )
