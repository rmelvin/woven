import datetime
import json
import time
from collections import Counter
from datetime import date
from operator import itemgetter

from django.db.models.aggregates import Sum
from rest_framework import permissions, views, viewsets, renderers
from rest_framework.decorators import detail_route
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from woven.models import Beacon, Site, Page, SiteUser, ViewStat
from woven.serializers import (
    SiteSerializer, PageSerializer, SiteUserSerializer, ViewStatSerializer,
)


# Performance considerations
# https://docs.djangoproject.com/en/1.9/ref/models/querysets/
# TODO: normalize url params according to db value


def _sortkey(item):
    return item[1]


class SiteViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list`, `retrieve` actions

    Additionally, we also provide an extra `highlight` action.
    """
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def get_serializer_class(self):
        """Return the serialization class to use"""
        return self.serializer_class

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        product = self.get_object()
        return Response(product.highlighted)

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class PageViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list`, `retrieve` actions.

    Additionally, we also provide an extra `highlight` action.
    """
    queryset = Page.objects.all()
    serializer_class = PageSerializer
#    permission_classes = (permissions.IsAuthenticated, )

    def get_serializer_class(self):
        """Return the serialization class to use"""
        return self.serializer_class

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        product = self.get_object()
        return Response(product.highlighted)

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    # TODO: add detail route pages/<id>/views and incorporate logic from
    # SiteViewsView and LeaderBoardView


class SiteUserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list`, `retrieve` actions.

    Additionally, we also provide an extra `highlight` action.
    """
    queryset = SiteUser.objects.all()
    serializer_class = SiteUserSerializer
#    permission_classes = (permissions.IsAuthenticated, )

    def get_serializer_class(self):
        """Return the serialization class to use"""
        return self.serializer_class

    @detail_route(renderer_classes=[renderers.StaticHTMLRenderer])
    def highlight(self, request, *args, **kwargs):
        product = self.get_object()
        return Response(product.highlighted)

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ViewStatViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally, we also provide an extra `highlight` action.
    """
    queryset = ViewStat.objects.all()
    serializer_class = ViewStatSerializer
#    permission_classes = (permissions.IsAuthenticated, MTSitePermissions, )

    def get_serializer_class(self):
        """Return the serialization class to use based
        on the incoming request type.

        Certain request types will mark certain fields
        as read only. (create vs update for example)
        """
#        http_method = self.request.method
#        if http_method in ['PUT', 'PATCH']:
#            serializer_class = SiteUpdateSerializer
#        elif http_method in ['POST']:
#            serializer_class = SiteCreateSerializer
#        else:
#            serializer_class = SiteSerializer

        return self.serializer_class

    def get_queryset(self):
        """Limit the `Site` listing depending on who you are logged on as

        As a superuser you see all Sites.

        As a customer you see all Sites you have access to based on your
        associated Billing service_ids
        if self.request.user.is_superuser:
            return Site.objects.all()
        elif self.request.auth and 'service_ids' in self.request.auth:
            return Site.objects.filter(
                    product__service_id__in=self.request.auth['service_ids'])
        else:
            return Site.objects.none()
        """
        return self.queryset

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):

        # Create a db entry for our new resource
        serializer.save()


class TopReferrersView(views.APIView):
    """
    (d) For a chosen page, what are the X most popular referrers?

    Request Params:
    :param url: (required) website url
    :type url: str
    :param count: (optional) number of referrers requested
                  default: 5
    :type count: int
    :param type: (optional) aggregation type (by frequency or page views)
        possible values: 'by_frequency' | 'by_views'
        default: 'by_frequency'
    :type type: str

    Response:
    [url1, url2, ..., urlN]
    """

    def get(self, request, format=None):
        url = request.query_params.get('url')
        # TODO: add trailing '/' to url if missing
        if not url:
            raise ParseError('must specify url')

        # TODO: put default count in settings
        count = int(request.query_params.get('count', 5))
        # TODO: validate count is coercable to number (try/catch int)

        # TODO: put type in settings
        count_type = self.request.query_params.get('type', 'by_frequency')
        valid_values = ('by_frequency', 'by_views')
        if count_type not in valid_values:
            raise ParseError(
                'invalid count type specified: {}. Please specify one of {}'
                .format(count_type, valid_values)
            )

        # get page referrers
        pagerefs = ViewStat.objects.filter(url__exact=url)\
            .exclude(pageref__exact='')\
            .values_list('pageref', flat=True)

        # (d)(1) Get the most frequent referrers
        if count_type == 'by_frequency':

            # compute histogram dictionary
            pagerefs_counts = Counter(pagerefs)

            # sort dict by count value
            sorted_pagerefs = sorted(pagerefs_counts.items(),
                                     key=itemgetter(1), reverse=True)

            # get top referrers
            referrers = [r[0]
                         for r in sorted_pagerefs[:count]
                         ]

        # (d)(2) Get the top-performing referrers by page views
        elif count_type == 'by_views':
            referred_views = []
            for pageref in set(pagerefs):
                q = ViewStat.objects.filter(pageref__exact=pageref)\
                    .aggregate(total_page_views=Sum('session_views'))
                if q.get('total_page_views'):
                    referred_views += [(pageref, q['total_page_views'])]

            referred_views.sort(key=_sortkey)
            referred_views.reverse()
            referrers = [r[0]
                         for r in referred_views[:count]
                         ]

        return Response(referrers)


class SurfersView(views.APIView):
    """
    (e) How many users are visiting multiple websites. What are they reading?
    Allow frontend to count number of users. Just return the data.

    Response:
    [ { sha1: [url1, .., urlM] }, ..., { shaN: [url1, .., urlP } ]
     """
    def get(self, request, format=None):
        """
        """
        surfers = [
            {
                str(u.id): Page.objects.filter(viewstats__usersha__exact=u.sha)
                .order_by('url')
                .values_list('url', flat=True)
            }
            for u in SiteUser.objects.all()
            if len(u.page_set.all()) > 1
        ]
        return Response(surfers)


class TopPagesView(views.APIView):
    """
    (c) For a chosen day, what are the X most popular pages?
    Question: count uproxx.com and brobible.com in the results or exclude?

    Request Params:
    :param day: (required) numerical weekday (Sun=1, Sat=7)
                default: 1
    :type day: int
    :param count: (optional) number of popular pages requested
                  default: 10
    :type count: int

    Response:
    [url1, url2, ..., urlN]
    """
    def get(self, request, format=None):
        """
        """
        count = int(request.query_params.get('count', 10))
        # TODO: validate count is coercable to number (try/catch int)
        day = int(request.query_params.get('day', 1))
        # TODO: validate day is coercable to number (try/catch int)
        # TODO: maybe allow day string and enumerate
        if day not in range(1, 8):
            raise ParseError(
                'invalid day specified: {}. Please specify 1-7'.format(day)
            )

        urls = ViewStat.objects.values_list('url', flat=True)
        page_views = []
        for url in list(set(urls)):
            q = ViewStat.objects.filter(url__exact=url) \
                .filter(lastutc__week_day=day) \
                .aggregate(total_page_views=Sum('session_views'))
            if q.get('total_page_views'):
                page_views += [(url, q['total_page_views'])]

        page_views.sort(key=_sortkey)
        page_views.reverse()
        pages = [p[0]
                 for p in page_views[:count]
                 ]
        return Response(pages)


class SiteViewsView(views.APIView):
    """
    (b) For a chosen website, what are the total page views for each day of
    the week?

    Request Params:
    :param url: (required) website url
    :type url: str

    Response:
    { 1: views1, 2: views2, ... , 7: viewsN }
    where Sun = 1, Sat = 7

    Reference:
    https://docs.djangoproject.com/en/1.9/ref/models/querysets/#week-day
    """
    def get(self, request, format=None):
        url = request.query_params.get('url')
        if not url:
            raise ParseError('must specify url')
        # TODO: add url validation

        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat']
        response = {}
        for (index, day) in list(enumerate(days, start=1)):
            q = ViewStat.objects.filter(url__iexact=url) \
                .filter(lastutc__week_day=index) \
                .aggregate(total_page_views=Sum('session_views'))
            response.update({
                index: q['total_page_views'] or 0  # 0 handles null
            })

        return Response(response)


class LeaderBoardView(views.APIView):
    """
    (a) What are the total page views on each website for the past X days?
    default X = 7

    # TODO: optional query maybe faster?
    sites = ViewStat.filter('lastutc__gt'=datetime.timedelta()).values_list('page__site_id', flat=True)
    x = ViewStat.objects.filter('page__site_id__in'=list(sites))
    response = {vs.page.site.name: vs.aggregate(total_page_views=Sum('session_page_views'))['total_page_views']
                for vs in x
                }

    Request Params:
    :param past_days: (optional) number of days in the past to query
                      default: 7
    :type past_days: int
    :param include_count: (optional) whether or not to include each url's view
                          count in the response
                          default: 1
    :type include_count: int (0 or 1)
    :param type: (optional) type of url, 'site' | 'page', default: 'page'
    :type type: str

    Response: in decreasing order of views
    [ [site1, num_views], [site2, num_views], ..., [siteN, num_views] ]

    If 'include_count' query parameter is 0, then response is just the urls
    (again, in decreasing order of views)
    [site1, site2, ..., siteN]
    """
    def get(self, request, format=None):
        past_days = int(request.query_params.get('past_days', 7))
        include_count = request.query_params.get('include_count', 1)
        valid_values = (1, 0)
        try:
            include_count = int(include_count)
        except:
            raise ParseError(
                'invalid include_count specified: {}. Please specify one of {}'
                .format(include_count, valid_values)
            )

        if include_count not in valid_values:
            raise ParseError(
                'invalid include_count specified: {}. Please specify one of {}'
                .format(include_count, valid_values)
            )

        site_type = request.query_params.get('type', 'page')
        valid_values = ('page', 'site')
        if site_type not in valid_values:
            raise ParseError(
                'invalid site type specified: {}. Please specify one of {}'
                .format(site_type, valid_values)
            )

        past_date_timestamp = time.time() - datetime.timedelta(
            days=past_days,
        ).total_seconds()

        stats = {}
        if site_type == 'site':
            sites = ViewStat.objects.values_list('site', flat=True)
            for site in list(set(sites)):
                if site not in [None, '']:
                    q = ViewStat.objects.filter(site__exact=site).filter(
                        lastutc__gt=date.fromtimestamp(past_date_timestamp),
                    ).aggregate(total_page_views=Sum('session_views'))
                    views = q['total_page_views']
                    if views:
                        stats.update({site: views})

        elif site_type == 'page':
            for page in Page.objects.all():
                q = page.viewstats.filter(
                    lastutc__gt=date.fromtimestamp(past_date_timestamp),
                ).aggregate(
                    total_page_views=Sum('session_views'),
                )
                views = q['total_page_views']
                if views:
                    stats.update({page.url: views})

        data = sorted(stats.items(),
                      key=itemgetter(1), reverse=True)
        if not include_count:
            data = [x[0] for x in data]

        return Response(data)


class LoadView(views.APIView):
    """
    Ingest additional rows of json data into db

    Performance:
    Avg response time for 1 row: 71 ms

    Request Params:
    :param json: rows of json data
    :type json: str (json format)

    Content-Type: application/json
    sample post data json:
    {"json":"[{\"request_log_id\":\"575df75000ff00ff2c06b38e02720001737e737465616d2d686f7573652d3932383135000132303136303431342d6d6173746572000106\",\"timestamp\":\"1465775952031\",\"gae_project_name\":\"steam-house-92815\",\"gae_instance_id\":\"00c61b117c63834847fa4eff6dddfe2b97e49b1091bc4f3d80438868f2e30cf7\",\"gae_version_id\":\"20160414-master\",\"status\":\"200\",\"remote_ip\":\"2602:306:cf35:640:a136:6b15:6bff:11b5\",\"version_tag\":\"v2\",\"json_block\":{\"ua\":\"Mozilla/5.0 (iPhone; CPU iPhone OS 8_1_2 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B440 Safari/600.1.4\",\"url\":\"http://reggie.com/life/article/omar-mateen-911-orlando-pledge-allegiance-isis/\",\"pageref\":\"https://www.google.com/\",\"ctime\":\"1465775949885\",\"ctz\":\"240\",\"urlparams\":\"\",\"pageid\":\"1192402\",\"usersha\":\"e549af8883dd4e0196341b978afdc984\",\"firstutc\":\"1465775949887\",\"lastutc\":\"1465775949887\",\"currentutc\":\"1465775949887\",\"lifetime_page_views\":\"100\",\"session_page_views\":\"100\",\"lifetime_sessions\":\"1\",\"_thredb\":\"reggie.e549af8883dd4e0l196341b978afdc985.1465775949887.14657759498.1467687170.10000.10000\"},\"b64_length\":\"500\"}]"}

    Content-Type: application/x-www-form-urlencoded
    sample post data json:
    (note: exclude user agent with ';' for now)
    {
        "request_log_id":"1",
        "timestamp":"123",
        "status":"200",
        ...,
        "json_block":{
            "url":"http://foobar.com",
            "pageref":"https://www.google.com/",
            ...,
            "lifetime_page_views":"100",
            "session_page_views":"100",
            "lifetime_sessions":"1",
            "_thredb":"reggie.e549af8883dd4e0l196341b978afdc985.1465775949887.1465775949887.1465775949887.100.100"},
        "b64_length":"500"
    }
    """
    def post(self, request, format=None):
        rows_json = request.data.get('json')
        if not rows_json:
            return

        rows_data = json.loads(rows_json)
        beacons = [Beacon(**row)
                   for row in rows_data
                   ]
        Beacon.objects.bulk_create(beacons)

        viewstats = [ViewStat(**parse_viewstat_data(row))
                     for row in rows_data
                     ]
        ViewStat.objects.bulk_create(viewstats)
        return Response('{} rows of data added'.format(len(rows_data)))


def parse_viewstat_data(data):
    """
    Ingest row data and save to db models.
    This creates all relational models on-the-fly (Site, Page, SiteUser)

    :param data: data structure representing row data from json
    :type data: dict
    :return response: dict data for creating ViewStat model
    :rtype response: dict
    """
    json_block_data = data.get('json_block')
    if not json_block_data:
        return

    if type(json_block_data) == str:
        json_block_data = json.loads(data['json_block'])

    url = json_block_data.get('url')
    response = {
        'url': url,
        'pageref': json_block_data.get('pageref'),
    }
    if json_block_data.get('_thredb') is None:
        return response

    parts = json_block_data['_thredb'].split('.')
    (site_name, usersha, firstutc, lastutc, currentutc, lifetime_views, session_views) = parts
    (site, site_created) = Site.objects.get_or_create(name=site_name)
    (page, page_created) = Page.objects.get_or_create(url=url, site=site)
    (user, user_created) = SiteUser.objects.get_or_create(sha=usersha)
    site.users.add(user)
    page.users.add(user)

    response.update({
        'page': page,
        'site': site_name,
        'usersha': usersha,
        'lastutc': date.fromtimestamp(float(lastutc[:10])),
        'lifetime_views': lifetime_views,
        'session_views': session_views,
    })
    return response
