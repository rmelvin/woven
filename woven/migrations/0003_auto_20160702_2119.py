# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('woven', '0002_auto_20160627_0400'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='pid',
            new_name='pageid',
        ),
    ]
