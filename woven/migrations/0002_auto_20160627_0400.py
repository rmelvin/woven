# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import date
import pprint

import json
from django.db import migrations, models


def parse_viewstat_data(apps, data):
    Site = apps.get_model('woven', 'Site')
    Page = apps.get_model('woven', 'Page')
    SiteUser = apps.get_model('woven', 'SiteUser')

    # e.g. uproxx.7a563c44853f491a9b663c310be9d5d5.1458268761573.1465223706629.1465223881473.80.58
    json_block_data = json.loads(data['json_block'])
    url = json_block_data.get('url')
    response = {
        'url': url,
        'pageref': json_block_data.get('pageref'),
    }
    if json_block_data.get('_thredb') is None:
        return response

    parts = json_block_data['_thredb'].split('.')
    (site_name, usersha, firstutc, lastutc, currentutc, lifetime_views, session_views) = parts
    (site, site_created) = Site.objects.get_or_create(name=site_name)
    (page, page_created) = Page.objects.get_or_create(url=url, site=site)
    (user, user_created) = SiteUser.objects.get_or_create(sha=usersha)
    site.users.add(user)
    page.users.add(user)

    response.update({
        'page': page,
        'site': site_name,
        'usersha': usersha,
        'lastutc': date.fromtimestamp(float(lastutc[:10])),
        'lifetime_views': lifetime_views,
        'session_views': session_views,
    })
    return response


def load_beacons(apps, schema_editor):
    # We can't import the model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Beacon = apps.get_model('woven', 'Beacon')
    ViewStat = apps.get_model('woven', 'ViewStat')

    # TODO: use json.load()
    # with open('data.json', 'r') as f:
    #     data = json.load(f)
    beacon_file = 'beacon_sample_data.json'
    f = open(beacon_file)
    beacons = [Beacon(**json.loads(line))
               for line in f
               ]
    Beacon.objects.bulk_create(beacons)

    # TODO: use seek instead
    # TODO: use list comp instead
    f = open(beacon_file)
    viewstats = []
    for line in f:
        data = json.loads(line)
        viewstats += [ViewStat(**parse_viewstat_data(apps, data))]
    ViewStat.objects.bulk_create(viewstats)


class Migration(migrations.Migration):

    dependencies = [
        ('woven', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_beacons),
    ]
