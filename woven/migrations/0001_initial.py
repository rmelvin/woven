# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Beacon',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('request_log_id', models.CharField(max_length=128, db_index=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('gae_project_name', models.CharField(max_length=255, blank=True)),
                ('gae_instance_id', models.CharField(max_length=255, blank=True)),
                ('gae_version_id', models.CharField(max_length=255, blank=True)),
                ('status', models.IntegerField(blank=True)),
                ('remote_ip', models.CharField(max_length=40, db_index=True, validators=[django.core.validators.URLValidator()], blank=True)),
                ('version_tag', models.CharField(max_length=16, blank=True)),
                ('json_block', models.CharField(max_length=2048, blank=True)),
                ('b64_length', models.IntegerField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'beacons',
                'verbose_name': 'beacon',
            },
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('pid', models.CharField(max_length=16, null=True, unique=True, blank=True)),
                ('url', models.CharField(max_length=1024, unique=True, validators=[django.core.validators.URLValidator()], blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=1024, unique=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='SiteUser',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('sha', models.CharField(max_length=255, unique=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ViewStat',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('url', models.CharField(max_length=1024, validators=[django.core.validators.URLValidator()], blank=True)),
                ('pageref', models.CharField(max_length=1024, validators=[django.core.validators.URLValidator()], blank=True)),
                ('site', models.CharField(max_length=1024, blank=True)),
                ('usersha', models.CharField(max_length=255, blank=True)),
                ('lastutc', models.DateTimeField(null=True, blank=True)),
                ('session_views', models.CharField(max_length=16, null=True, blank=True)),
                ('lifetime_views', models.CharField(max_length=16, null=True, blank=True)),
                ('page', models.ForeignKey(null=True, to='woven.Page', related_name='viewstats')),
            ],
            options={
                'ordering': ['session_views'],
            },
        ),
        migrations.AddField(
            model_name='site',
            name='users',
            field=models.ManyToManyField(to='woven.SiteUser'),
        ),
        migrations.AddField(
            model_name='page',
            name='site',
            field=models.ForeignKey(null=True, to='woven.Site', related_name='pages'),
        ),
        migrations.AddField(
            model_name='page',
            name='users',
            field=models.ManyToManyField(to='woven.SiteUser'),
        ),
    ]
