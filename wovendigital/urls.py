"""wovendigital URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter

from woven import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'sites', views.SiteViewSet)
router.register(r'pages', views.PageViewSet)
# TODO: implement /pages/<id>/topreferrers
router.register(r'users', views.SiteUserViewSet)
router.register(r'viewstats', views.ViewStatViewSet)

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^surfers/$', views.SurfersView.as_view()),
    url(r'^top_referrers/$', views.TopReferrersView.as_view()),
    url(r'^top_pages/$', views.TopPagesView.as_view()),
    url(r'^site_views/$', views.SiteViewsView.as_view()),
    url(r'^leaderboard/$', views.LeaderBoardView.as_view()),
    url(r'^load/$', views.LoadView.as_view()),
#    url(r'^api-auth/', include('rest_framework.urls',
#                               namespace='rest_framework')),
#    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
#    url(r'^api-token-verify/', 'rest_framework_jwt.views.verify_jwt_token'),
#    url(r'^swagger.json$', never_cache(serve), {
#        'document_root': settings.BASE_DIR,
#        'path': settings.SWAGGER_FILE,
#    }),
]
